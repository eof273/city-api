# city-api

Промежуточный слой между приложением [AngularWeather](https://github.com/eof273/angular-weather) и базой городов `openweathermap`, развернутой на `mlab`;

## api

Предоставляет метод:

```
/cities?query=[search qurey]
```

Возвращает список из 5 городов формата:

```
_id:        string  - mongo id
id:         number  - id города для openweathermap
name:       string  - Название города
country:    string  - Двухбуквенный код страны
```
