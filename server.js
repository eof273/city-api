const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');

const CITIES_COLLECTION = 'cities';

const app = express();
app.use(bodyParser.json());

let db;

mongodb.MongoClient.connect(process.env.MONGODB_URI, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  db = database;
  console.log('Database connection ready');

  const server = app.listen(process.env.PORT || 8080, function () {
    const port = server.address().port;
    console.log('App now running on port', port);
  });
});

function handleError(res, reason, message, code) {
  console.log('ERROR: ' + reason);
  res.status(code || 500).json({'error': message});
}

app.get('/cities', function (req, res) {
  const query = req.query.query;

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

  db.collection(CITIES_COLLECTION)
    .find({
      name: new RegExp(query, 'i')
    })
    .limit(5)
    .toArray(function (err, docs) {
      if (err) {
        handleError(res, err.message, 'Failed to get contacts.');
      } else {
        res.status(200).json(docs);
      }
    })
  ;
});
